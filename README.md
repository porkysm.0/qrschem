# qrschem

a really shitty litematica qr code schematic generator. very important

![Preview](./assets/preview.png)

## Usage

```
usage: main.py [-h] [--error-correction {L,M,Q,H}] [--orientation {vertical,flat}] [--pixel-scale PIXEL_SCALE] [--block-black BLOCK_BLACK] [--block-white BLOCK_WHITE] [--block-backfill BLOCK_BACKFILL] content output_file

Make funny minecraft qr codes (feds can't read them)

positional arguments:
  content               The content to embed in the QR code (e.g. real)
  output_file           The output schematic (e.g. cum.litematic)

optional arguments:
  -h, --help            show this help message and exit
  --error-correction {L,M,Q,H}
                        The error correction used for the QR code. (default: L)
  --orientation {vertical,flat}
                        The orientation to create the schematic in. (default: vertical)
  --pixel-scale PIXEL_SCALE
                        The width of a single pixel in blocks. (default: 1)
  --block-black BLOCK_BLACK
                        The block to use for black pixels. (default: minecraft:black_wool)
  --block-white BLOCK_WHITE
                        The block to use for white pixels. (default: minecraft:white_wool)
  --block-backfill BLOCK_BACKFILL
                        The block to use for a backfill. (default: None)
```