import argparse
from enum import Enum
from typing import Tuple

import qrcode
from PIL.Image import Image
from litemapy import Region, BlockState, Schematic

ERROR_CORRECTION_TRANSLATION = {
    "L": qrcode.ERROR_CORRECT_L,
    "M": qrcode.ERROR_CORRECT_M,
    "Q": qrcode.ERROR_CORRECT_Q,
    "H": qrcode.ERROR_CORRECT_H
}


class SchematicOrientation(Enum):
    VERTICAL = "vertical"
    FLAT = "flat"


class QRCodeGenerator:
    def __init__(
            self,
            block_black="minecraft:black_wool",
            block_white="minecraft:white_wool",
            pixel_size=1,
            border_size=1,
            block_backfill=None,
            error_correction="L",
            orientation=SchematicOrientation.VERTICAL
    ):
        self._block_black = block_black
        self._block_white = block_white
        self._pixel_size = pixel_size
        self._border_size = border_size
        self._block_backfill = block_backfill
        self._error_correction = ERROR_CORRECTION_TRANSLATION[error_correction.upper()]
        self._orientation = orientation

    def _translate_position(self, x: int, y: int, z: int) -> Tuple[int, int, int]:
        if self._orientation == SchematicOrientation.VERTICAL:
            return x, y, z

        return x, z, y

    def _generate_qr_code(self, content: str) -> Image:
        qr = qrcode.QRCode(
            box_size=self._pixel_size,
            border=self._border_size,
            error_correction=self._error_correction
        )

        qr.add_data(content)

        return qr.make_image().convert("L")

    def generate_schematic(self, content: str) -> Schematic:
        qr_img = self._generate_qr_code(content)

        schematic_depth = 1 if self._block_backfill is None else 2

        reg = Region(
            0, 0, 0, *self._translate_position(qr_img.width, qr_img.height, schematic_depth)
        )
        schem = reg.as_schematic(name="qr", author="feds", description=f"Content: {content}")

        for x in range(qr_img.width):
            for y in range(qr_img.height):
                avg = qr_img.getpixel((x, y))

                # Coordinate conversion
                rel_y = qr_img.height - 1 - y

                reg.setblock(
                    *self._translate_position(
                        x,
                        rel_y,
                        0 if self._block_backfill is None else 1
                    ),
                    block=BlockState(
                        self._block_black if avg == 0 else self._block_white
                    )
                )

                if self._block_backfill is not None:
                    # Apply backfill
                    reg.setblock(
                        *self._translate_position(
                            x,
                            rel_y,
                            0
                        ),
                        block=BlockState(self._block_backfill)
                    )

        return schem


def main():
    parser = argparse.ArgumentParser(
        description="Make funny minecraft qr codes (feds can't read them)",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )

    parser.add_argument(
        "content",
        help="The content to embed in the QR code (e.g. real)"

    )
    parser.add_argument(
        "output_file",
        help="The output schematic (e.g. cum.litematic)"
    )

    parser.add_argument(
        "--error-correction",
        type=str.upper,
        choices=["L", "M", "Q", "H"],
        default="L",
        help="The error correction used for the QR code."
    )

    parser.add_argument(
        "--orientation",
        type=str.lower,
        choices=["vertical", "flat"],
        default="vertical",
        help="The orientation to create the schematic in."
    )

    parser.add_argument(
        "--pixel-scale",
        default=1,
        help="The width of a single pixel in blocks."
    )

    parser.add_argument(
        "--block-black",
        default="minecraft:black_wool",
        help="The block to use for black pixels."
    )

    parser.add_argument(
        "--block-white",
        default="minecraft:white_wool",
        help="The block to use for white pixels."
    )

    parser.add_argument(
        "--block-backfill",
        help="The block to use for a backfill."
    )

    parsed = parser.parse_args()

    QRCodeGenerator(
        block_black=parsed.block_black,
        block_white=parsed.block_white,
        block_backfill=parsed.block_backfill,
        error_correction=parsed.error_correction,
        orientation=SchematicOrientation(parsed.orientation),
        pixel_size=parsed.pixel_scale
    ).generate_schematic(parsed.content).save(parsed.output_file)


if __name__ == "__main__":
    main()
